#!/bin/sh

rm -rf public/*

PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE apk list -I`

which envsubst || apk add gettext

mkdir -p public

README=`cat README.md` \
DATE=`date` \
PACKAGE_LIST="$PACKAGE_LIST" \
envsubst < template.html > public/index.html
