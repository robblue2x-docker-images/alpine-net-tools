FROM alpine:latest

RUN apk --no-cache add \
  bash \
  bind-tools \
  nano \
  net-tools \
  netcat-openbsd \
  nmap \
  openssh-client \
  openssl \
  sshfs
